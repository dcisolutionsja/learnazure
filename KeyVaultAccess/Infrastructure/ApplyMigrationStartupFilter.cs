﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KeyVaultAccess.Infrastructure
{
    public class ApplyMigrationStartupFilter : IStartupFilter
    {
        IServiceProvider serviceProvider;
        public ApplyMigrationStartupFilter(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next)
        {
            return builder =>
            {
                using (var scope = this.serviceProvider.CreateScope())
                {
                    var context = scope.ServiceProvider.GetService<SecureDBContext>();
                    context.Database.Migrate();
                }
                next(builder);
            };
        }
    }
}
