﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KeyVaultAccess.Models;
using Bogus;
using KeyVaultAccess.Context.Domain;

namespace KeyVaultAccess.Controllers
{
    public class HomeController : Controller
    {
        SecureDBContext context;

        public HomeController(SecureDBContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            
            return View(this.context.Customer.ToList());
        }

        public IActionResult AddNewCustomer()
        {
            var fakeCustomer = new Faker<Customer>();
            fakeCustomer.RuleFor(x=> x.Id, y => y.System.Random.Guid())
                        .RuleFor(x => x.FirstName, y => y.Name.FirstName())
                        .RuleFor(x => x.LastName, y => y.Name.LastName())
                        .RuleFor(x => x.MiddleName, y => y.Name.FirstName());

            this.context.Customer.Add(fakeCustomer.Generate());
            this.context.SaveChanges();
            return RedirectToAction("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
