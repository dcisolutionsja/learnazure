﻿using System;
using KeyVaultAccess.Context.Domain;
using KeyVaultAccess.Context.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.DataEncryption;
using Microsoft.EntityFrameworkCore.Metadata;

namespace KeyVaultAccess
{
    public partial class SecureDBContext : DbContext
    {
        IEncryptionProvider provider;
        public DbSet<Customer> Customer { get; set; }

        public SecureDBContext(IEncryptionProvider provider, DbContextOptions<SecureDBContext> options)
            : base(options)
        {
            this.provider = provider;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                // NOTE: 3. This will not be used because we configure the options in the startup
                optionsBuilder.UseNpgsql("HOST=localhost;DB=SecureDB;UID=dbuser;PWD=p@ssword1");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");
            modelBuilder.ApplyConfiguration(new CustomerMapping());
        }
    }
}
