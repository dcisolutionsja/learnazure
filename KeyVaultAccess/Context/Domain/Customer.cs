﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KeyVaultAccess.Context.Domain
{
    public class Customer
    {
        public Guid Id { get; set; }

        [Encrypted]
        public string FirstName { get; set; }
        [Encrypted]
        public string MiddleName { get; set; }
        [Encrypted]
        public string LastName { get; set; }
    }
}
