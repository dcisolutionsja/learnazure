param (
	[string] $name = $Args[0]
)

dotnet ef migrations add "$name" -o Context/Migrations -p "KeyVaultAccess/KeyVaultAccess.csproj" -s "KeyVaultAccess/KeyVaultAccess.csproj"