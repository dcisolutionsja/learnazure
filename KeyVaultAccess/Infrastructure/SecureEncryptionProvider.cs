﻿using Microsoft.EntityFrameworkCore.DataEncryption;
using Microsoft.EntityFrameworkCore.DataEncryption.Providers;
using Microsoft.Extensions.Configuration;
using System;
using System.Buffers.Text;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyVaultAccess.Infrastructure
{
    public class SecureEncryptionProvider : IEncryptionProvider
    {
        IEncryptionProvider internalEncryptionProvider;
        public SecureEncryptionProvider(IConfiguration configuration)
        {
            var key = configuration["SecretEncryptionKey"];
            var keyBytes = Encoding.ASCII.GetBytes(key);

            var iv = configuration["SecretEncryptionIV"];
            var ivBytes = Encoding.ASCII.GetBytes(iv);

            this.internalEncryptionProvider = new AesProvider(keyBytes, ivBytes);

        }
        public string Decrypt(string dataToDecrypt)
        {
            return this.internalEncryptionProvider.Decrypt(dataToDecrypt);
        }

        public string Encrypt(string dataToEncrypt)
        {
            return this.Encrypt(dataToEncrypt);
        }
    }
}
