if (([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{    
  Set-Location $PSScriptRoot
  "Running as admin in $PSScriptRoot"
}
else
{
  "NOT running as an admin!" 
  Start-Process powershell -WorkingDirectory $PSScriptRoot -Verb runAs -ArgumentList "-noprofile -noexit -file $PSCommandPath" 
  return "Script re-started with admin privileges in another shell. This one will now exit."
}
"Installing Package Manager"

Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force

"Installing Az Module"
Install-Module Az -SkipPublisherCheck -AllowClobber #-AllowPrerelease	
Import-Module Az 

"Connecting to Azure (This may require user interaction)"
Connect-AzAccount

New-AzResourceGroup -Name "LearnAzure" -Location "East US" -Force

"Creating KeyVault: LearnAzureKeyVault-dci (Note that these have to be universally unique so if you don't have access to this you will have to change it.)"
New-AzKeyVault -Name "LearnAzureKeyVault-dci" -ResourceGroupName "LearnAzure" -Location "East US"

$connectionString = ConvertTo-SecureString -String "HOST=localhost;DB=SecureDB;UID=dbuser;PWD=p@ssword1" -AsPlainText -Force

Set-AzKeyVaultSecret -Name "connectionString" -VaultName "LearnAzureKeyVault-dci" -SecretValue $connectionString


	